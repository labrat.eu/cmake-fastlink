# CMake Fastlink

Just a small CMake package to clean bloated CMake targets. Useful for brainless frameworks like ROS.

----

Made by Max Yvon Zimmermann (myzh)
