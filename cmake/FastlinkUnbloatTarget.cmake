cmake_minimum_required(VERSION 3.15.0)

function(fastlink_unbloat_target)
  set(options NO_LIBRARIES NO_INCLUDE_DIRS)
  set(oneValueArgs)
  set(multiValueArgs TARGETS)
  cmake_parse_arguments(INT "${options}" "${oneValueArgs}" "${multiValueArgs}" ${ARGN})

  # Handle missing targets argument.
  if(NOT DEFINED INT_TARGETS)
    message(FATAL_ERROR "No targets specified.")
  endif()

  # Filter out duplicate libraries.
  if(NOT ${INT_NO_LIBRARIES})
    foreach(TARGET IN LISTS INT_TARGETS)
      get_target_property(ALL_LIBRARIES ${TARGET} LINK_LIBRARIES)
      list(REMOVE_DUPLICATES ALL_LIBRARIES)
      set_target_properties(${TARGET} PROPERTIES LINK_LIBRARIES "${ALL_LIBRARIES}")
    endforeach()
  endif()

  # Filter out duplicate include directories.
  if(NOT ${INT_NO_INCLUDE_DIRS})
    foreach(TARGET IN LISTS INT_TARGETS)
      get_target_property(ALL_INCLUDE_DIRS ${TARGET} INCLUDE_DIRECTORIES)
      list(REMOVE_DUPLICATES ALL_INCLUDE_DIRS)
      set_target_properties(${TARGET} PROPERTIES INCLUDE_DIRECTORIES "${ALL_INCLUDE_DIRS}")
    endforeach()
  endif()
endfunction(fastlink_unbloat_target)
